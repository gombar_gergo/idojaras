//
//  CityTableViewCell.swift
//  Idojaras
//
//  Created by Gergo Gombar on 2019. 03. 21..
//  Copyright © 2019. com.gg. All rights reserved.
//

import UIKit

class CityTableViewCell: UITableViewCell {
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var countyNameLabel: UILabel!
}
