//
//  CityDetailsViewController.swift
//  Idojaras
//
//  Created by Gergo Gombar on 2019. 03. 20..
//  Copyright © 2019. com.gg. All rights reserved.
//

import UIKit

protocol CityDetailsDisplayLogic: class {
    func displayTitle(title: String)
    func displayCityDetails(response: ResponseWeather)
    func displayDownloadError(error: Error?)
}

class CityDetailsViewController: UIViewController, CityDetailsDisplayLogic {
    var interactor: CityDetailsBusinessLogic?
    var router: (NSObjectProtocol & CityDetailsRoutingLogic & CityDetailsDataPassing)?

    override func viewDidLoad() {
        super.viewDidLoad()
        interactor?.getCityName()
        interactor?.getCityDetails()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?){
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup() {
        let viewController = self
        let interactor = CityDetailsInteractor()
        let presenter = CityDetailsPresenter()
        let router = CityDetailsRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var weatherLabel: UILabel!
    
    func displayCityDetails(response: ResponseWeather) {
        hideLoader()
        
        let weatherDescription = "Weather description: \(response.weather.first?.description ?? "") \n"
        let temperatureInCelsius = kelvinToCelsius(kelvin: response.main.temp)
        let temperatureString = "Temperature: \(temperatureInCelsius) Celsius \n"
        let windSpeedString = "Wind speed: \(response.wind.speed) m/s \n"
        //...
        
        weatherLabel.text = weatherDescription + temperatureString + windSpeedString
    }
    
    private func kelvinToCelsius(kelvin: Double) -> Double {
        let celsius = kelvin - 273.15
        let celsiusRounded = (celsius*100).rounded()/100
        return celsiusRounded
    }
    
    func displayDownloadError(error: Error?) {
        hideLoader()
        errorLabel.text = "Error\n\(error?.localizedDescription ?? "")"
    }
    
    func displayTitle(title: String) {
        self.title = title
    }
    
    private func hideLoader() {
        loader.isHidden = true
    }
}
