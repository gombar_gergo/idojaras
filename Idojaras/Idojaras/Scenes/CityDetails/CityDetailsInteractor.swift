//
//  CityDetailsInteractor.swift
//  Idojaras
//
//  Created by Gergo Gombar on 2019. 03. 20..
//  Copyright © 2019. com.gg. All rights reserved.
//

import UIKit

protocol CityDetailsBusinessLogic {
    func getCityDetails()
    func getCityName()
}

protocol CityDetailsDataStore {
    var city: City! { get set}
}

class CityDetailsInteractor: CityDetailsBusinessLogic, CityDetailsDataStore {
    var presenter: CityDetailsPresentationLogic?
    
    var city: City!
    var urlStringForCityWeather = "https://api.openweathermap.org/data/2.5/weather?q={CITY},hu&APPID=bfd70532def6d658f4ef0da5dba53b45"
    
    func getCityDetails() {
        urlStringForCityWeather = urlStringForCityWeather.replacingOccurrences(of: "{CITY}", with: city.name)
        
        WeatherDownloader.downloadWeather(urlString: urlStringForCityWeather) { (response, error) in
            if let response = response, error == nil {
                DispatchQueue.main.async {
                    self.presenter?.presentCityDetails(response: response)
                }
            }
            else {
                DispatchQueue.main.async {
                    self.presenter?.presentDownloadError(error: error)
                }
            }
        }
    }
    
    func getCityName() {
        presenter?.presentTitle(title: city.name)
    }
}
