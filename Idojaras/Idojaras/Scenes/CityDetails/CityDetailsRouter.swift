//
//  CityDetailsRouter.swift
//  Idojaras
//
//  Created by Gergo Gombar on 2019. 03. 20..
//  Copyright © 2019. com.gg. All rights reserved.
//

import UIKit

@objc protocol CityDetailsRoutingLogic{
    func routeToCitiesList(segue: UIStoryboardSegue?)
}

protocol CityDetailsDataPassing {
    var dataStore: CityDetailsDataStore? { get }
}

class CityDetailsRouter: NSObject, CityDetailsRoutingLogic, CityDetailsDataPassing {
    weak var viewController: CityDetailsViewController?
    var dataStore: CityDetailsDataStore?
    
    func routeToCitiesList(segue: UIStoryboardSegue?) {
    }
}
