//
//  CityDetailsPresenter.swift
//  Idojaras
//
//  Created by Gergo Gombar on 2019. 03. 20..
//  Copyright © 2019. com.gg. All rights reserved.
//

import UIKit

protocol CityDetailsPresentationLogic {
    func presentCityDetails(response: ResponseWeather)
    func presentDownloadError(error: Error?)
    func presentTitle(title: String)
}

class CityDetailsPresenter: CityDetailsPresentationLogic {

    weak var viewController: CityDetailsDisplayLogic?
    
    func presentCityDetails(response: ResponseWeather) {
        viewController?.displayCityDetails(response: response)
    }
    
    func presentDownloadError(error: Error?) {
        viewController?.displayDownloadError(error: error)
    }
    
    func presentTitle(title: String) {
        viewController?.displayTitle(title: title)
    }
}
