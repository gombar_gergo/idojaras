//
//  CitiesListPresenter.swift
//  Idojaras
//
//  Created by Gergo Gombar on 2019. 03. 20..
//  Copyright © 2019. com.gg. All rights reserved.
//

import UIKit

protocol CitiesListPresentationLogic {
    func presentFetchedCities(citiesList: [City])
}

class CitiesListPresenter: CitiesListPresentationLogic {
    weak var viewController: CitiesListDisplayLogic?
    
    func presentFetchedCities(citiesList: [City]) {
        viewController?.displayFetchedCities(citiesList: citiesList)
    }
}
