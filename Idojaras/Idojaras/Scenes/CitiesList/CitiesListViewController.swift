//
//  CitiesListViewController.swift
//  Idojaras
//
//  Created by Gergo Gombar on 2019. 03. 20..
//  Copyright © 2019. com.gg. All rights reserved.
//

import UIKit

protocol CitiesListDisplayLogic: class {
    func displayFetchedCities(citiesList: [City])
}

class CitiesListViewController: UITableViewController, CitiesListDisplayLogic {
    var interactor: CitiesListBusinessLogic?
    var router: (NSObjectProtocol & CitiesListRoutingLogic & CitiesListDataPassing)?
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?){
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder){
        super.init(coder: aDecoder)
        setup()
    }

    func setup() {
        let viewController = self
        let interactor = CitiesListInteractor()
        let presenter = CitiesListPresenter()
        let router = CitiesListRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor?.fetchCities()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        router?.routeToShowDetails(segue: segue)
    }

    var displayedCities: [City] = []
    
    func displayFetchedCities(citiesList: [City]) {
        displayedCities = citiesList
        tableView.reloadData()
    }

    // MARK: tableView dataSource
    override func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return displayedCities.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let displayedCity = displayedCities[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityTableViewCell", for: indexPath) as! CityTableViewCell
        cell.cityNameLabel?.text = displayedCity.name
        cell.countyNameLabel?.text = displayedCity.county
        
        return cell
    }
}
