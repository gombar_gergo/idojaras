//
//  CitiesListRouter.swift
//  Idojaras
//
//  Created by Gergo Gombar on 2019. 03. 20..
//  Copyright © 2019. com.gg. All rights reserved.
//

import UIKit

@objc protocol CitiesListRoutingLogic{
    func routeToShowDetails(segue: UIStoryboardSegue?)
}

protocol CitiesListDataPassing {
    var dataStore: CitiesListDataStore? { get }
}

class CitiesListRouter: NSObject, CitiesListRoutingLogic, CitiesListDataPassing {
    weak var viewController: CitiesListViewController?
    var dataStore: CitiesListDataStore?
    
    func routeToShowDetails(segue: UIStoryboardSegue?) {
        if let segue = segue {
            let destinationViewController = segue.destination as! CityDetailsViewController
            var destinationDataStore = destinationViewController.router!.dataStore!
            passDataToCityDetails(source: dataStore!, destination: &destinationDataStore)
        }
        else {
            let destinationViewController = viewController?.storyboard?.instantiateViewController(withIdentifier: "CityDetailsViewController") as! CityDetailsViewController
            var destinationDataStore = destinationViewController.router!.dataStore!
            passDataToCityDetails(source: dataStore!, destination: &destinationDataStore)
            navigateToCityDetails(source: viewController!, destination: destinationViewController)
        }
    }
    
    // MARK: Passing data
    func passDataToCityDetails(source: CitiesListDataStore, destination: inout CityDetailsDataStore) {
        let selectedRow = viewController?.tableView.indexPathForSelectedRow?.row
        destination.city = source.cities?[selectedRow!]
    }
    
    // MARK: Navigation
    func navigateToCityDetails(source: CitiesListViewController, destination: CityDetailsViewController) {
        source.navigationController?.pushViewController(destination, animated: true)
    }
}
