//
//  CitiesListInteractor.swift
//  Idojaras
//
//  Created by Gergo Gombar on 2019. 03. 20..
//  Copyright © 2019. com.gg. All rights reserved.
//

import UIKit

protocol CitiesListBusinessLogic {
    func fetchCities()
}

protocol CitiesListDataStore {
    var cities: [City]? {get}
}

class CitiesListInteractor: CitiesListBusinessLogic, CitiesListDataStore {
    var presenter: CitiesListPresentationLogic?
    var cities: [City]?
    
    func fetchCities() {
        cities = mockCities()
        presenter?.presentFetchedCities(citiesList: cities!)
    }
    
    func mockCities() -> [City] {
        let city01 = City(name: "Dabas", county: "Pest megye")
        let city02 = City(name: "Ajka", county: "Veszprém megye")
        let city03 = City(name: "Gyula", county: "Békés megye")
        let city04 = City(name: "Kardos", county: "Békés megye")
        let city05 = City(name: "IlyenNincs", county: "Bács-Kiskun")
        
        return [city01, city02, city03, city04, city05]
    }
}
