//
//  WeatherDownloader.swift
//  Idojaras
//
//  Created by Gergo Gombar on 2019. 03. 22..
//  Copyright © 2019. com.gg. All rights reserved.
//

import Foundation

class WeatherDownloader {
    
    class func downloadWeather(urlString: String, _ completion: @escaping (ResponseWeather?, Error?) -> ()) {
        let url = URL(string: urlString)!
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                completion(nil, error)
            }
            
            guard let data = data else {
                completion(nil, nil)
                return
            }
            
            do {
                let decodedData = try JSONDecoder().decode(ResponseWeather.self, from: data)
                completion(decodedData, nil)
            } catch let jsonError {
                completion(nil, jsonError)
            }
        }.resume()
    }
}
