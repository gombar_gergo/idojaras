//
//  City.swift
//  Idojaras
//
//  Created by Gergo Gombar on 2019. 03. 20..
//  Copyright © 2019. com.gg. All rights reserved.
//

import Foundation
import CoreLocation

struct City {
    var name: String
    var county: String
    
    init(name: String, county: String) {
        self.name = name
        self.county = county
    }
}
