//
//  ResponseWeather.swift
//  Idojaras
//
//  Created by Gergo Gombar on 2019. 03. 20..
//  Copyright © 2019. com.gg. All rights reserved.
//

import Foundation
import CoreLocation

struct ResponseWeather: Codable {
    var coord: Coord
    var weather: [Weather]
    var main: Main
    var wind: Wind
    var clouds: Cloud
    var dt: Int
    var sys: Sys
    var id: Int
    var name: String
}

// MARK: Supporting models
struct Coord: Codable {
    var lon: Double
    var lat: Double
}

struct Weather: Codable {
    var id: Int
    var main: String
    var description: String
    var icon: String
}

struct Main: Codable {
    var temp: Double
    var pressure: Int
    var humidity: Int
    var temp_min: Double
    var temp_max: Double
}

struct Wind: Codable {
    var speed: Double
    var deg: Int
}

struct Cloud: Codable {
    var all: Int
}

struct Sys: Codable {
    var type: Int
    var id: Int
    var message: Double
    var country: String
    var sunrise: Int
    var sunset: Int
}
